package ashberrysoft.syvan_andrey.onlineshop.network;

/**
 * Created by andrey on 2/26/14.
 */
public interface NetworkCallback<R> {
    public void sendNetworkResult(R result);

    public void onLoading(boolean value);
}
