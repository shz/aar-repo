package ashberrysoft.syvan_andrey.onlineshop.network;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
/**
 * Created by andrey on 2/3/14.
 * // Class get data from server
 */
public abstract class NetworkRequestHandler<T>
        extends AsyncTask<Void, Void, T> {

//TODO поднять вверх класса.
    public static enum RequestType {
        GET, POST;
    }

    String response = null;
    T result;
    private NetworkCallback<T> mCallback;
    private Context mContext;
    private List<Integer> mList;
    private String mURL;

    private RequestType mMethod;

    public NetworkRequestHandler(Context context, String url, RequestType method) {
        mContext = context;
        mURL = url;
        mMethod = method;
    }

    /*
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    @Override
    protected T doInBackground(Void... voids) {

        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse;

            if (mMethod == RequestType.POST) {
                httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(mURL);
                httpPost.setEntity(new UrlEncodedFormEntity(getPostParams()));
                httpResponse = httpClient.execute(httpPost);
                httpEntity = httpResponse.getEntity();
            }

            if (mMethod == RequestType.GET) {
                HttpGet httpGet = new HttpGet(mURL);
                httpResponse = httpClient.execute(httpGet);
                httpEntity = httpResponse.getEntity();
            }
            response = EntityUtils.toString(httpEntity);
            result = parseResult(response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    /*
         * Making service call
         * @url - url to make request
         * @method - http request method
         * @params - http request params
         * */
    public abstract T parseResult(String str) throws JSONException;

    public abstract List<NameValuePair> getPostParams();

    @Override
    protected void onPostExecute(T result) {
        super.onPostExecute(result);

        //TODO лучше юзать условие чем ретурн.
        if (mCallback != null) {
            mCallback.sendNetworkResult(result);
            mCallback.onLoading(false);
        } else {
            Toast.makeText(mContext, "Error result from server", Toast.LENGTH_SHORT).show();
        }
    }

    public void execute(NetworkCallback<T> callBack) {
        //TODO неправильный порядок.
        mCallback = callBack;
        mCallback.onLoading(true);
        execute();
    }

    public List getList() {
        return mList;
    }
}
