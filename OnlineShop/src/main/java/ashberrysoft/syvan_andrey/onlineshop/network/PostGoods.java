package ashberrysoft.syvan_andrey.onlineshop.network;

import android.content.Context;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.dao.ReturnNetworkResponse;

/**
 * Created by andrey on 2/21/14.
 * Class to work with list of purchased goods
 */
//TODO Описание класа где?
public class PostGoods extends NetworkRequestHandler<ReturnNetworkResponse> {
    //TODO модификат доступа
    private List<Integer> mList;

    public PostGoods(Context context, String url, List<Integer> list) {
        super(context, url, RequestType.POST);
        mList = list;
    }

    private String prepareList() {
        StringBuilder str = new StringBuilder();
        str.append("[");
        for (int i = 0; i < mList.size(); i++) {
            if (i != mList.size() - 1) {
                str.append(mList.get(i) + ",");
            } else {
                str.append(mList.get(i));
            }
        }
        str.append("]");
        return str.toString();
    }

    @Override
    public ReturnNetworkResponse parseResult(String str) throws JSONException {
        return new ReturnNetworkResponse(response);
    }

    @Override
    public List<NameValuePair> getPostParams() {
        List<NameValuePair> params = new ArrayList<NameValuePair>(1);
        params.add(new BasicNameValuePair("buy", prepareList()));
        return params;
    }

    public List getList() {
        return mList;
    }
}
