package ashberrysoft.syvan_andrey.onlineshop.network;

import android.content.Context;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.dao.Goods;

/**
 * Created by andrey on 2/4/14.
 * // Class to return parse result from server
 */

public class GoodsRequest extends NetworkRequestHandler<List<Goods>> {

    private static final String TAG_DATA = "data";

    public GoodsRequest(Context context, String url) {
        super(context, url, RequestType.GET);
    }

    @Override
    public List<Goods> parseResult(String response) throws JSONException {
        JSONArray data;
        JSONObject jsonObj = new JSONObject(response);
        data = jsonObj.getJSONArray(TAG_DATA);
        List arrayList = new ArrayList();
        for (int i = 0; i < data.length(); i++) {
            JSONObject jsonObject = data.getJSONObject(i);
            Goods goodsObj;
            try {
                goodsObj = new Goods(jsonObject);
                arrayList.add(goodsObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    @Override
    public List<NameValuePair> getPostParams() {
        return null;
    }
}
