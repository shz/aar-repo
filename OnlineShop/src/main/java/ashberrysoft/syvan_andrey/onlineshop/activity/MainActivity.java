package ashberrysoft.syvan_andrey.onlineshop.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import ashberrysoft.syvan_andrey.onlineshop.R;
import ashberrysoft.syvan_andrey.onlineshop.fragment.GoodsFragment;
//TODO убрать из коментариев лишние слешы.
//TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний
//TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний
//TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний
//TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний
//TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний
// TODO пакеты называютя только маленькими латинскими буквами, и желательно без подчеркиваний

/**
 * Main class. Contains logic to work load progress
 */
//TODO не тот тип активити. Здесь нет сапорт экшн бара
public class MainActivity extends ActionBarActivity {

    public static final String ACTION_LOAD = "ashberrysoft.syvan_andrey.onlineshop.ACTION_LOAD";
    public static final String EXTRA_LOAD_VALUE = "ashberrysoft.syvan_andrey.onlineshop.EXTRA_LOAD_VALUE";
    private BroadcastReceiver mReciver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_LOAD)) {
                LoadMSG msg = LoadMSG.values()[intent.getIntExtra(EXTRA_LOAD_VALUE, 0)];
                switch (msg) {
                    case Load_Start:
                        showLoadingProgress(true);
                        break;
                    case Load_Finish:
                        showLoadingProgress(false);
                        break;
                    case Network_Error:
                        Toast.makeText(MainActivity.this, R.string.checkedInternet, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        }
    };
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new GoodsFragment()).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mReciver, new IntentFilter(ACTION_LOAD));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReciver);
        super.onPause();
    }

    public void showLoadingProgress(boolean value) {
        if (value) {
            if (mProgress == null) {
                mProgress = new ProgressDialog(MainActivity.this);
                mProgress.setCanceledOnTouchOutside(false);
                mProgress.setMessage(getString(R.string.load));
            }
            mProgress.show();
        } else {
            if (mProgress != null) {
                mProgress.dismiss();
                mProgress = null;
            }
        }
    }

    public enum LoadMSG {
        Load_Start, Load_Finish, Network_Error
    }
}