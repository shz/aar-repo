package ashberrysoft.syvan_andrey.onlineshop.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import ashberrysoft.syvan_andrey.onlineshop.R;
//TODO сделай базовый фрагмент в котором будет методы получения апликейшена, запуск фрагментов.

/**
 * Created by andrey on 3/4/14.
 */
public class SettingsFragment extends OSBaseFragment implements CompoundButton.OnCheckedChangeListener {

    public CheckBox mIsShowBuyDialog;
    public CheckBox mIsShowRemoveDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //TODO R попроще пропишы.
        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);

        setHasOptionsMenu(true);

        mIsShowBuyDialog = (CheckBox) rootView.findViewById(R.id.ifBuyCheckBox);
        mIsShowRemoveDialog = (CheckBox) rootView.findViewById(R.id.ifRemoveCheckBox);

        mIsShowBuyDialog.setOnCheckedChangeListener(this);
        mIsShowRemoveDialog.setOnCheckedChangeListener(this);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        switch (compoundButton.getId()) {
            case R.id.ifBuyCheckBox:
                if (isChecked) {
                    mIsShowBuyDialog.setChecked(true);
                    //TODO Вынести в одну переменную application
                    getApplicationContext().setShowBuyDialog(true);
                } else {
                    getApplicationContext().setShowBuyDialog(false);
                }
                break;
            case R.id.ifRemoveCheckBox:
                if (isChecked) {
                    getApplicationContext().setShowRemoveDialog(true);
                } else {
                    getApplicationContext().setShowRemoveDialog(false);
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsShowBuyDialog.setChecked(getApplicationContext().getShowBuyDialog());
        mIsShowRemoveDialog.setChecked(getApplicationContext().getShowRemoveDialog());
    }
}
