package ashberrysoft.syvan_andrey.onlineshop.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import ashberrysoft.syvan_andrey.onlineshop.R;
import ashberrysoft.syvan_andrey.onlineshop.system.OSApplication;

/**
 * Created by andrey on 13.03.14.
 */
public class OSBaseFragment extends Fragment {

    protected void startFragment(Fragment fragment, Fragment frag) {
        final FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        if (frag != null) {
            mFragmentTransaction.addToBackStack(frag.getClass().getSimpleName());
        }
        mFragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left,
                R.anim.out_to_right);
        mFragmentTransaction.replace(R.id.container, fragment);
        mFragmentTransaction.commit();
    }

    protected OSApplication getApplicationContext(){
        return (OSApplication) getActivity().getApplicationContext();
    }
}




