package ashberrysoft.syvan_andrey.onlineshop.fragment;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.json.JSONException;

import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.R;
import ashberrysoft.syvan_andrey.onlineshop.activity.MainActivity;
import ashberrysoft.syvan_andrey.onlineshop.adapters.GoodsBasketCursorAdapter;
import ashberrysoft.syvan_andrey.onlineshop.adapters.GoodsCursorAdapter;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsBasketTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.dao.ReturnNetworkResponse;
import ashberrysoft.syvan_andrey.onlineshop.network.NetworkCallback;
import ashberrysoft.syvan_andrey.onlineshop.network.PostGoods;
import ashberrysoft.syvan_andrey.onlineshop.system.GoodsContentProvider;
import ashberrysoft.syvan_andrey.onlineshop.system.OSApplication;

/**
 * Created by andrey on 2/6/14.
 * Fragment class to work with adapters, lists, and loaders wich works with adapters
 */
public class GoodsFragment extends OSBaseFragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        GoodsCursorAdapter.OnButtonClickListener,
        GoodsBasketCursorAdapter.OnButtonBasketClickListener, View.OnTouchListener {

    public static final int LOADER_GOODS = 1;
    public static final int LOADER_GOODS_BASKET = 2;
    public ViewFlipper mViewFlipper;
    private ActionBar bar;
    private float mLastX;
    private ListView mGoodsList;
    private ListView mGoodsListBasket;
    private GoodsCursorAdapter mAdapter;
    private GoodsBasketCursorAdapter mBasketAdapter;
    private TextView mGoodsTotalPrice;
    private TextView mPurchased;
    private AlertDialog.Builder mBuilder;

    private int mPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new GoodsCursorAdapter(getActivity(), null, null);
        mBasketAdapter = new GoodsBasketCursorAdapter(getActivity(), null, null);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mGoodsList = (ListView) rootView.findViewById(R.id.fm_goodsList);
        mGoodsListBasket = (ListView) rootView.findViewById(R.id.fm_goodsListBasket);
        mGoodsTotalPrice = (TextView) rootView.findViewById(R.id.totalPriceOfGoods);
        mPurchased = (TextView) rootView.findViewById(R.id.purchased);
        mViewFlipper = (ViewFlipper) rootView.findViewById(R.id.fm_goodsFlipper);
        mGoodsList.setAdapter(mAdapter);
        mGoodsListBasket.setAdapter(mBasketAdapter);
        mAdapter.setListener(this);
        mBasketAdapter.setBasketButtonListener(this);
        mViewFlipper.setOnTouchListener(this);

        mViewFlipper.setDisplayedChild(mPosition);

        bar = ((MainActivity) getActivity()).getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(false);

        changeActionBarOnStart();

        return rootView;
    }

    public void changeActionBarOnStart() {
        if (mViewFlipper.getDisplayedChild() == 0) {
            changeActionBar(0);
        } else {
            changeActionBar(1);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                changeDataList();
                return true;
            case R.id.goodsMenu:
                changeDataList();
                return true;
            case R.id.settingsMenu:
                startFragment(new SettingsFragment(), this);
                changeActionBar(2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(LOADER_GOODS, null, this);
        getLoaderManager().initLoader(LOADER_GOODS_BASKET, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPosition = mViewFlipper.getDisplayedChild();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        switch (i) {
            case LOADER_GOODS:
                return new CursorLoader(getActivity(),
                        GoodsContentProvider.PROVIDER_CONTENT_URI, null, null, null, null);
            case LOADER_GOODS_BASKET:
                return new CursorLoader(getActivity(),
                        GoodsContentProvider.BASKET_PROVIDER_CONTENT_URI, null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        switch (cursorLoader.getId()) {
            case LOADER_GOODS:
                mAdapter.changeCursor(cursor);
                break;
            case LOADER_GOODS_BASKET:
                updateTotalPrice(cursor);
                mBasketAdapter.changeCursor(cursor);
                cursor.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        updateTotalPrice(mBasketAdapter.getCursor());
                    }
                });
                break;
        }
    }

    public void changeDataList() {
        if (mViewFlipper.getDisplayedChild() == 1) {
            mViewFlipper.setDisplayedChild(0);
            changeActionBar(0);
        } else {
            mViewFlipper.setDisplayedChild(1);
            changeActionBar(1);
        }
    }

    public void updateTotalPrice(Cursor cursor) {
        int total = 0;
        int totalPurchased = 0;
        while (cursor.moveToNext()) {
            if (GoodsBasketTableColumns.getGoodsBasketBought(cursor) == 1) {
                totalPurchased += Integer.parseInt(GoodsTableColumns.getGoodsPrice(cursor))
                        * GoodsBasketTableColumns.getGoodsBasketCount(cursor);
            }
            total += Integer.parseInt(GoodsTableColumns.getGoodsPrice(cursor))
                    * GoodsBasketTableColumns.getGoodsBasketCount(cursor);
        }
        mGoodsTotalPrice.setText(String.valueOf(total));
        mPurchased.setText(String.valueOf(totalPurchased));
    }

    private void changeActionBar(int i) {
        if (i == 0) {
            bar.setBackgroundDrawable(getResources().getDrawable(R.layout.layers));
            getActivity().setTitle("Goods");
            bar.setDisplayHomeAsUpEnabled(false);
            bar.setHomeButtonEnabled(false);
        } else if (i == 1) {
            bar.setBackgroundDrawable(getResources().getDrawable(R.layout.layers_two));
            getActivity().setTitle("Basket");
            bar.setCustomView(R.drawable.bas);
            bar.setDisplayHomeAsUpEnabled(true);
        } else {
            getActivity().setTitle("Settings");
            bar.setBackgroundDrawable(getResources().getDrawable(R.layout.layers_three));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            // when user first touches the screen to swap
            case MotionEvent.ACTION_DOWN: {

                mLastX = motionEvent.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {

                float currentX = motionEvent.getX();
                // if left to right swipe on screen
                if (mLastX < currentX) {

                    // If no more View/Child to flip

                    if (mViewFlipper.getDisplayedChild() == 0)
                        break;
                    changeActionBar(0);
                    // set the required Animation type to ViewFlipper
                    // The Next screen will come in form Left and current Screen will go OUT from Right
                    mViewFlipper.setInAnimation(getActivity(), R.anim.in_from_left);
                    mViewFlipper.setOutAnimation(getActivity(), R.anim.out_to_right);
                    // Show the next Screen
                    mViewFlipper.showNext();
                }
                // if right to left swipe on screen
                if (mLastX > currentX) {

                    if (mViewFlipper.getDisplayedChild() == 1)
                        break;
                    changeActionBar(1);
                    // set the required Animation type to ViewFlipper
                    // The Next screen will come in form Right and current Screen will go OUT from Left
                    mViewFlipper.setInAnimation(getActivity(), R.anim.in_from_right);
                    mViewFlipper.setOutAnimation(getActivity(), R.anim.out_to_left);
                    // Show The Previous Screen
                    mViewFlipper.showPrevious();
                }
                break;
            }
        }
        return true;
    }

    @Override
    public void addToBasket(int id, int count) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GoodsBasketTableColumns.KEY_GOODS_ID, id);
        contentValues.put(GoodsBasketTableColumns.KEY_GOODS_BASKET_COUNT, count);
        contentValues.put(GoodsBasketTableColumns.KEY_BOUGHT, 0);
        getActivity().getContentResolver().insert(GoodsContentProvider.BASKET_PROVIDER_CONTENT_URI,
                contentValues);
        if (((OSApplication) getActivity().getApplication()).isShowBuyDialog) {

            mBuilder = new AlertDialog.Builder(getActivity());
            mBuilder.setTitle(R.string.dialogTitleGoods);
            // Message
            mBuilder.setMessage(R.string.dialogMessageGoods);
            // Icon
            mBuilder.setIcon(android.R.drawable.ic_dialog_info);
            mBuilder.setPositiveButton(R.string.okGoods, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            mBuilder.setNegativeButton(R.string.dialogNegativeBtn, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mViewFlipper.setInAnimation(getActivity(), R.anim.in_from_right);
                    mViewFlipper.setOutAnimation(getActivity(), R.anim.out_to_left);
                    mViewFlipper.setDisplayedChild(1);
                    changeActionBar(1);
                }
            });
            mBuilder.show();
        }
    }

    public void goodsRemove(final int id, int count) {
        if (((OSApplication) getActivity().getApplication()).isShowRemoveDialog) {
            mBuilder = new AlertDialog.Builder(getActivity());
            mBuilder.setTitle(R.string.removeDialogTitle);
            // Message
            mBuilder.setMessage(R.string.removeConfirmDialogMessage);
            // Icon
            mBuilder.setIcon(android.R.drawable.ic_dialog_info);
            mBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().getContentResolver().delete(GoodsContentProvider.BASKET_PROVIDER_CONTENT_URI,
                            GoodsTableColumns.KEY_ID + "=?", new String[]{id + ""});
                }
            });
            mBuilder.setNegativeButton(R.string.no, null);
            mBuilder.show();
        } else {
            getActivity().getContentResolver().delete(GoodsContentProvider.BASKET_PROVIDER_CONTENT_URI,
                    GoodsTableColumns.KEY_ID + "=?", new String[]{id + ""});
        }
    }

    @Override
    public void goodsBuy(final int id, final List<Integer> list) throws JSONException {
        PostGoods postGoods = new PostGoods(getActivity(), GoodsContentProvider.URL, list);
        //TODO Не надо так!
        mBuilder = new AlertDialog.Builder(getActivity());
        postGoods.execute(new NetworkCallback<ReturnNetworkResponse>() {
            @Override
            public void sendNetworkResult(ReturnNetworkResponse result) {
                mBuilder.setPositiveButton(R.string.ok, null);
                if (isNetworkAvailable()) {
                    if (result.getCode() == 1) {
                        onLoading(true);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(GoodsBasketTableColumns.KEY_BOUGHT, 1);
                        getActivity().getContentResolver().update(GoodsContentProvider.BASKET_PROVIDER_CONTENT_URI,
                                contentValues, GoodsTableColumns.KEY_ID + "=?",
                                new String[]{id + ""});
                        mBuilder.setTitle(R.string.successful);
                        mBuilder.setMessage(String.format("%s: %d", result.getMsg(), result.getPrice()));
                        //TODO Зачем?
                        //TODO одинаковые строки можно выносить выше условия
                        mBuilder.show();
                    } else {
                        mBuilder.setTitle(R.string.failed);
                        //TODO Зачем?
                        mBuilder.setMessage(result.getMsg());
                        mBuilder.show();
                    }
                } else {
                    sendLoadStatus(MainActivity.LoadMSG.Network_Error);
                }
            }

            @Override
            public void onLoading(boolean value) {
                ((MainActivity) getActivity()).showLoadingProgress(value);
            }
        });
    }

    public boolean isNetworkAvailable() {
        if (getActivity() != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null;
        }
        return false;
    }

    public void sendLoadStatus(MainActivity.LoadMSG load) {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_LOAD);
        intent.putExtra(MainActivity.EXTRA_LOAD_VALUE, load.ordinal());
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

}

