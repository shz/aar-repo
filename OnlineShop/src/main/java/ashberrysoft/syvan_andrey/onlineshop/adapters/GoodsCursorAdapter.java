package ashberrysoft.syvan_andrey.onlineshop.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import ashberrysoft.syvan_andrey.onlineshop.view.GoodsListItemView;

/**
 * Created by andrey on 2/5/14.
 * // Adapter to work with goods list
 */

public class GoodsCursorAdapter extends CursorAdapter {

    private OnButtonClickListener mListener;

    public GoodsCursorAdapter(Context context, Cursor c, OnButtonClickListener onButtonClick) {
        super(context, c);
        mListener = onButtonClick;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        GoodsListItemView goodsListView = new GoodsListItemView(context);
        goodsListView.setListener(mListener);
        return goodsListView;
    }

    public void setListener(OnButtonClickListener onButtonClick) {
        mListener = onButtonClick;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((GoodsListItemView) view).bindView(cursor);
    }

    //TODO По правилам названий у интерфейсов должно в названии быть слово указывающее. Напимер OnButtonClickListener
//TODO По правилам названий у интерфейсов должно в названии быть слово указывающее. Напимер OnButtonClickListener
//TODO По правилам названий у интерфейсов должно в названии быть слово указывающее. Напимер OnButtonClickListener
//TODO По правилам названий у интерфейсов должно в названии быть слово указывающее. Напимер OnButtonClickListener
//TODO По правилам названий у интерфейсов должно в названии быть слово указывающее. Напимер OnButtonClickListener
    public interface OnButtonClickListener {
        void addToBasket(int id, int count);
    }
}
