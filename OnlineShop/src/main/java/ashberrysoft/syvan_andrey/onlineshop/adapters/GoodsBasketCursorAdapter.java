package ashberrysoft.syvan_andrey.onlineshop.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;

import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.view.GoodsBasketListItemView;

/**
 * Created by andrey on 2/6/14.
 * // Adapter to work with goods basket
 */

public class GoodsBasketCursorAdapter extends CursorAdapter {

    private OnButtonBasketClickListener mButtonBasketListener;

    public GoodsBasketCursorAdapter(Context context, Cursor c, OnButtonBasketClickListener onButtonBasketClick) {
        super(context, c);
        mButtonBasketListener = onButtonBasketClick;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        GoodsBasketListItemView goodsBasketListItemView = new GoodsBasketListItemView(context);
        goodsBasketListItemView.setBasketButtonListener(mButtonBasketListener);
        return goodsBasketListItemView;
    }

    public void setBasketButtonListener(OnButtonBasketClickListener onButtonBasketClick) {
        mButtonBasketListener = onButtonBasketClick;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((GoodsBasketListItemView) view).bindData(cursor);
    }

    public interface OnButtonBasketClickListener {
        void goodsRemove(int id, int count);

        void goodsBuy(int id, List<Integer> list) throws JSONException;
    }
}
