package ashberrysoft.syvan_andrey.onlineshop.system;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.activity.MainActivity;
import ashberrysoft.syvan_andrey.onlineshop.dao.Goods;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.network.GoodsRequest;
import ashberrysoft.syvan_andrey.onlineshop.network.NetworkCallback;

/**
 * Created by andrey on 2/4/14.
 * // Content provider class
 */

public class GoodsContentProvider extends ContentProvider {

    public static final String AUTHORITY = "ashberrysoft.syvan_andrey.provider";
    public static final Uri PROVIDER_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/goods");
    public static final Uri BASKET_PROVIDER_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/goodsBasket");
    public static final int GOODS = 1;
    public static final int GOODS_BASKET = 2;
    private static final String GOODS_TABLE = "goods";
    private static final String GOODS_BASKET_TABLE = "goodsBasket";
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, GOODS_TABLE, GOODS);
        uriMatcher.addURI(AUTHORITY, GOODS_BASKET_TABLE, GOODS_BASKET);
    }

    public static String URL = "http://api.shz.in.ua/";
    private MySQLiteHelper mDbHelper;
    private SQLiteDatabase sqlDB;

    @Override
    public boolean onCreate() {
        mDbHelper = new MySQLiteHelper(getContext());
        sqlDB = mDbHelper.getWritableDatabase();
        return false;
    }

    public boolean isNetworkAvailable() {
        if (getContext() != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null;
        }
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor;
        int uriType = uriMatcher.match(uri);

        switch (uriType) {
            case GOODS:
                queryBuilder.setTables(MySQLiteHelper.GOODS_TABLE);
                cursor = queryBuilder.query(mDbHelper.getReadableDatabase(),
                        projection, selection, selectionArgs, null, null, sortOrder);
                if (cursor.getCount() == 0) {
                    if (isNetworkAvailable()) {
                        new GoodsRequest(getContext(), URL).execute(new NetworkCallback<List<Goods>>() {
                            @Override
                            public void sendNetworkResult(List<Goods> dataList) {
                                ContentValues[] masContentValues = new ContentValues[dataList.size()];
                                for (int i = 0; i < dataList.size(); i++) {
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(GoodsTableColumns.KEY_ID, dataList.get(i).getId());
                                    contentValues.put(GoodsTableColumns.KEY_NAME, dataList.get(i).getName());
                                    contentValues.put(GoodsTableColumns.KEY_PRICE, dataList.get(i).getPrice());
                                    contentValues.put(GoodsTableColumns.KEY_IMG, dataList.get(i).getImg());
                                    contentValues.put(GoodsTableColumns.KEY_IS, dataList.get(i).getIs());
                                    masContentValues[i] = contentValues;
                                }
                                bulkInsert(PROVIDER_CONTENT_URI, masContentValues);
                            }

                            @Override
                            public void onLoading(boolean value) {
                                sendLoadStatus(value ? MainActivity.LoadMSG.Load_Start :
                                        MainActivity.LoadMSG.Load_Finish);
                            }
                        });
                    } else {
                        sendLoadStatus(MainActivity.LoadMSG.Network_Error);
                    }
                }
                break;
            case GOODS_BASKET:
                queryBuilder.setTables(MySQLiteHelper.GOODS_BASKET_TABLE);
                cursor = sqlDB.rawQuery(GoodsTableColumns.queryToJoin, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI");
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int uriType = uriMatcher.match(uri);
        String table;
        long id;
        switch (uriType) {
            case GOODS:
                table = MySQLiteHelper.GOODS_TABLE;
                break;
            case GOODS_BASKET:
                table = MySQLiteHelper.GOODS_BASKET_TABLE;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        id = sqlDB.insert(table,
                null, contentValues);
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(table + "/" + id);
    }

    public void sendLoadStatus(MainActivity.LoadMSG load) {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_LOAD);
        intent.putExtra(MainActivity.EXTRA_LOAD_VALUE, load.ordinal());
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case GOODS_BASKET:
                // delete all the records of the table
                count = sqlDB.delete(GOODS_BASKET_TABLE, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table = null;
        switch (uriMatcher.match(uri)) {
            case GOODS:
                table = MySQLiteHelper.GOODS_TABLE;
                break;
            case GOODS_BASKET:
                table = MySQLiteHelper.GOODS_BASKET_TABLE;
                break;
        }
        sqlDB.beginTransaction();
        for (ContentValues contentValues : values) {
            sqlDB.insertWithOnConflict(table, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            getContext().getContentResolver().notifyChange(uri, null);
        }
        sqlDB.setTransactionSuccessful();
        sqlDB.endTransaction();
        getContext().getContentResolver().notifyChange(uri, null);
        return values.length;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = uriMatcher.match(uri);
        SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
        int rowsUpdated;
        switch (uriType) {
            case GOODS:
                rowsUpdated = sqlDB.update(MySQLiteHelper.GOODS_TABLE,
                        values,
                        selection,
                        selectionArgs);
                break;
            case GOODS_BASKET:
                rowsUpdated = sqlDB.update(MySQLiteHelper.GOODS_BASKET_TABLE,
                        values,
                        selection,
                        selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
