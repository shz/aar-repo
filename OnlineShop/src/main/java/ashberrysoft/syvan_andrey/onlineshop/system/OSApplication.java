package ashberrysoft.syvan_andrey.onlineshop.system;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;

/**
 * Created by andrey on 11.03.14.
 */
public class OSApplication extends Application {

    private final String KEY_SETTINGS_FILE = "settings_file";
    private final String KEY_BUY_DIALOG = "isBuy";
    private final String KEY_REMOVE_DIALOG = "isRemove";

    public boolean isShowBuyDialog;
    public boolean isShowRemoveDialog;

    private SharedPreferences sp;

    @Override
    public void onCreate() {
        super.onCreate();
        //TODO ключи выноси в константы
        sp = getSharedPreferences(KEY_SETTINGS_FILE, Activity.MODE_PRIVATE);
        //TODO Здесь кеще должно быть получение сохраненных данных

    }

    public boolean getShowBuyDialog() {
        return sp.getBoolean(KEY_BUY_DIALOG, isShowBuyDialog);
    }

    public void setShowBuyDialog(boolean isBuy) {
        isShowBuyDialog = isBuy;
        sp.edit().putBoolean(KEY_BUY_DIALOG, isShowBuyDialog).commit();
    }

    public boolean getShowRemoveDialog() {
        return sp.getBoolean(KEY_REMOVE_DIALOG, isShowRemoveDialog);
    }

    public void setShowRemoveDialog(boolean isRemove) {
        isShowRemoveDialog = isRemove;
        sp.edit().putBoolean(KEY_REMOVE_DIALOG, isShowRemoveDialog).commit();
    }

}
