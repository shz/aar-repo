package ashberrysoft.syvan_andrey.onlineshop.system;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsBasketTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsTableColumns;

/**
 * Created by andrey on 2/4/14.
 * // Data base class
 */

public class MySQLiteHelper extends SQLiteOpenHelper {
    // Goods table name
    public static final String GOODS_TABLE = "goods";
    private static final String CREATE_GOODS_TABLE = "create table " + GOODS_TABLE + " ( " +
            GoodsTableColumns.KEY_ID + " INTEGER PRIMARY KEY, " +
            GoodsTableColumns.KEY_NAME + " TEXT, " +
            GoodsTableColumns.KEY_PRICE + " INTEGER, " +
            GoodsTableColumns.KEY_IMG + " TEXT, " +
            GoodsTableColumns.KEY_IS + " INTEGER )";
    public static final String GOODS_BASKET_TABLE = "goodsBasket";
    private static final String CREATE_GOODS_BASKET_TABLE = "create table " +
            GOODS_BASKET_TABLE + "( " +
            GoodsTableColumns.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            GoodsBasketTableColumns.KEY_GOODS_ID + " INTEGER, " +
            GoodsBasketTableColumns.KEY_BOUGHT + " INTEGER, " +
            GoodsBasketTableColumns.KEY_GOODS_BASKET_COUNT + " INTEGER )";
    public static final String DB_NAME = "goods_db";
    public static final int DB_VERSION = 18;

    public MySQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_GOODS_TABLE);
        db.execSQL(CREATE_GOODS_BASKET_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GOODS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + GOODS_BASKET_TABLE);
        onCreate(db);
    }
}

