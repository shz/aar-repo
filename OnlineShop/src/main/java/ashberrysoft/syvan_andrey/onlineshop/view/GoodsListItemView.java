package ashberrysoft.syvan_andrey.onlineshop.view;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import ashberrysoft.syvan_andrey.onlineshop.R;
import ashberrysoft.syvan_andrey.onlineshop.adapters.GoodsCursorAdapter;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.system.GoodsContentProvider;

/**
 * Created by andrey on 2/10/14.
 * // Class to display goods list
 */

public class GoodsListItemView extends LinearLayout implements View.OnClickListener {

    private static final String URL_IMAGE = GoodsContentProvider.URL + "%s";
    private GoodsCursorAdapter.OnButtonClickListener mListener;
    private TextView mName;
    private TextView mPrice;
    private EditText mCount;
    private TextView mGoodsAvailable;
    private Button mBtnMoveToBasket;
    private ImageView mGoodsImage;
    private int mId;
    private boolean mAvailable;

    public GoodsListItemView(Context context) {
        super(context);
        inflate(context, R.layout.goods_field, this);
        mName = (TextView) findViewById(R.id.goodsName);
        mPrice = (TextView) findViewById(R.id.goodsPrice);
        mCount = (EditText) findViewById(R.id.goodsCount);
        mBtnMoveToBasket = (Button) findViewById(R.id.btnMoveToBasket);
        mGoodsImage = (ImageView) findViewById(R.id.goodsImage);
        mGoodsAvailable = (TextView) findViewById(R.id.goodsAvailable);
        mBtnMoveToBasket.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mAvailable) {
            if (!mCount.getText().toString().equalsIgnoreCase("") &&
                    !mCount.getText().toString().equalsIgnoreCase("0")) {
                mListener.addToBasket(mId, Integer.parseInt(mCount.getText().toString()));
                mCount.setText("");
            } else {
                Toast.makeText(getContext(), R.string.putGoodsCount, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), R.string.goodsNotAvailable, Toast.LENGTH_SHORT).show();
        }
    }

    public void bindView(Cursor cursor) {
        mAvailable = (GoodsTableColumns.getGoodsIs(cursor)) == 1;
        mId = GoodsTableColumns.getGoodsId(cursor);
        mName.setText(GoodsTableColumns.getGoodsName(cursor));
        mPrice.setText(GoodsTableColumns.getGoodsPrice(cursor));
        UrlImageViewHelper.setUrlDrawable(mGoodsImage,
                String.format(URL_IMAGE,
                        GoodsTableColumns.getGoodsImg(cursor)),
                R.drawable.ic_launcher
        );
        mGoodsAvailable.setText(mAvailable ?
                R.string.yes : R.string.no);

    }

    public void setListener(GoodsCursorAdapter.OnButtonClickListener onButtonClick) {
        mListener = onButtonClick;
    }
}
