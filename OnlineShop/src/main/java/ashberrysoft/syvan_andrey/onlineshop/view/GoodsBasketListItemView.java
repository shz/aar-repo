package ashberrysoft.syvan_andrey.onlineshop.view;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONException;

import java.util.List;

import ashberrysoft.syvan_andrey.onlineshop.R;
import ashberrysoft.syvan_andrey.onlineshop.adapters.GoodsBasketCursorAdapter;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsBasketTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.dao.GoodsTableColumns;
import ashberrysoft.syvan_andrey.onlineshop.system.GoodsContentProvider;

/**
 * Created by andrey on 2/6/14.
 * // Class to display goods list in basket
 */

public class GoodsBasketListItemView extends LinearLayout implements View.OnClickListener {

    private static final String URL_IMAGE = GoodsContentProvider.URL + "%s";
    private final TextView mTVName;
    private final TextView mTVPrice;
    private final TextView mTVCount;
    private final TextView mTVStatus;
    private final Button mBtnRemove;
    private final Button mBtnBuy;
    private final ImageView mGlvGoodsImage;
    private GoodsBasketCursorAdapter.OnButtonBasketClickListener mButtonBasketListener;
    private int mId;
    private Cursor mCursor;
    private List<Integer> mMasId;

    public GoodsBasketListItemView(Context context) {
        super(context);
        inflate(context, R.layout.goods_list_view, this);
        mTVName = (TextView) findViewById(R.id.glv_goodsName);
        mTVPrice = (TextView) findViewById(R.id.glv_goodsPrice);
        mTVCount = (TextView) findViewById(R.id.glv_goodsCount);
        mTVStatus = (TextView) findViewById(R.id.glv_goodsStatus);
        mGlvGoodsImage = (ImageView) findViewById(R.id.glv_goodsImage);
        mBtnRemove = (Button) findViewById(R.id.glv_btnRemove);
        mBtnBuy = (Button) findViewById(R.id.glv_btnBuy);
        mBtnRemove.setOnClickListener(this);
        mBtnBuy.setOnClickListener(this);
    }

    public void bindData(Cursor cursor) {
        mCursor = cursor;
        mId = GoodsBasketTableColumns.getGoodsBasketId(cursor);
        mMasId = GoodsBasketTableColumns.sendMasId(mCursor);
        mTVCount.setText(String.valueOf(GoodsBasketTableColumns.getGoodsBasketCount(cursor)));
        mTVName.setText(GoodsTableColumns.getGoodsName(cursor));
        mTVPrice.setText(GoodsTableColumns.getGoodsPrice(cursor));
        UrlImageViewHelper.setUrlDrawable(mGlvGoodsImage,
                String.format(URL_IMAGE,
                        GoodsTableColumns.getGoodsImg(cursor)),
                R.drawable.ic_launcher);
        if (GoodsBasketTableColumns.getGoodsBasketBought(mCursor) != 1) {
            mTVStatus.setText(R.string.not_bought);
            mBtnBuy.setVisibility(VISIBLE);
        } else {
            mTVStatus.setText(R.string.bought);
            mBtnBuy.setVisibility(INVISIBLE);
        }
    }

    public void setBasketButtonListener(GoodsBasketCursorAdapter.OnButtonBasketClickListener onButtonBasketClick) {
        mButtonBasketListener = onButtonBasketClick;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.glv_btnBuy:
                try {
                    mButtonBasketListener.goodsBuy(mId, mMasId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.glv_btnRemove:
                mButtonBasketListener.goodsRemove(mId, Integer.parseInt(mTVCount.getText().toString()));
                break;
        }
    }
}
