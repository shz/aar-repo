package ashberrysoft.syvan_andrey.onlineshop.dao;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrey on 2/14/14.
 * // Class with key of basket table columns of data base
 */
public class GoodsBasketTableColumns {

    public static final String KEY_ID = "_id";
    public static final String KEY_GOODS_ID = "goods_id";
    public static final String KEY_BOUGHT = "bought";
    public static final String KEY_GOODS_BASKET_COUNT = "count";

    public static int getGoodsBasketId(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(GoodsBasketTableColumns.KEY_ID));
    }

    public static int getGoodsBasketBought(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(GoodsBasketTableColumns.KEY_BOUGHT));
    }

    public static int getGoodsBasketCount(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(GoodsBasketTableColumns.KEY_GOODS_BASKET_COUNT));
    }

    public static int getId(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(GoodsBasketTableColumns.KEY_GOODS_ID));
    }

    public static List<Integer> sendMasId(Cursor cursor) {
        int goodsId = getId(cursor);
        int count = getGoodsBasketCount(cursor);
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < count; i++) {
            list.add(goodsId);
        }
        return list;
    }
}
