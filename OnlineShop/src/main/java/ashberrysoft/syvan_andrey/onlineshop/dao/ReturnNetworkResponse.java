package ashberrysoft.syvan_andrey.onlineshop.dao;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andrey on 2/25/14.
 */

public class ReturnNetworkResponse {

    private static final String TAG_MSG = "msg";
    private static final String TAG_CODE = "code";
    private int mPrice;
    private String mMsg;
    private int mCode;

    public ReturnNetworkResponse(String postResponse) throws JSONException {
        JSONObject jsonObject = new JSONObject(postResponse);
        mCode = jsonObject.getInt(TAG_CODE);
        mMsg = jsonObject.getString(TAG_MSG);
        //TODO надо делать проверку на наличие элемента а не только на код.
        if (jsonObject.has(GoodsTableColumns.KEY_PRICE))
            mPrice = jsonObject.getInt(GoodsTableColumns.KEY_PRICE);
    }

    public int getCode() {
        return mCode;
    }

    public String getMsg() {
        return mMsg;
    }

    public int getPrice() {
        return mPrice;
    }
}
