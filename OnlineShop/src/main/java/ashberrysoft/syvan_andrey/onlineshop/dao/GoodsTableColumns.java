package ashberrysoft.syvan_andrey.onlineshop.dao;

import android.database.Cursor;

import ashberrysoft.syvan_andrey.onlineshop.system.MySQLiteHelper;

/**
 * Created by andrey on 2/12/14.
 * // Class with key table columns of data base and variable to join the tables
 */

public class GoodsTableColumns {
    // Goods Table Columns names
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PRICE = "price";
    public static final String KEY_IMG = "img";
    public static final String KEY_IS = "_is";

    public static final String queryToJoin =
            "SELECT " +
                    MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_ID + " AS _id2, " +
                    MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_NAME + ", " +
                    MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_PRICE + ", " +
                    MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_IMG + ", " +
                    MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_IS + ", " +
                    MySQLiteHelper.GOODS_BASKET_TABLE + "." + GoodsBasketTableColumns.KEY_ID + ", " +
                    MySQLiteHelper.GOODS_BASKET_TABLE + "." + GoodsBasketTableColumns.KEY_GOODS_ID + ", " +
                    MySQLiteHelper.GOODS_BASKET_TABLE + "." + GoodsBasketTableColumns.KEY_BOUGHT + ", " +
                    MySQLiteHelper.GOODS_BASKET_TABLE + "." + GoodsBasketTableColumns.KEY_GOODS_BASKET_COUNT +
                    " FROM " + MySQLiteHelper.GOODS_BASKET_TABLE +
                    " LEFT JOIN " + MySQLiteHelper.GOODS_TABLE +
                    " ON " + MySQLiteHelper.GOODS_BASKET_TABLE + "." + GoodsBasketTableColumns.KEY_GOODS_ID + " = "
                    + MySQLiteHelper.GOODS_TABLE + "." + GoodsTableColumns.KEY_ID;

    public static int getGoodsId(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(KEY_ID));
    }

    public static String getGoodsName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_NAME));
    }

    public static String getGoodsPrice(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_PRICE));
    }

    public static String getGoodsImg(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_IMG));
    }

    public static int getGoodsIs(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(KEY_IS));
    }
}
