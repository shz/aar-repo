package ashberrysoft.syvan_andrey.onlineshop.dao;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andrey on 2/4/14.
 * //Class with parsing data tags, their variables, getters and setters
 */

public class Goods {

    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_PRICE = "price";
    private static final String TAG_IMG = "img";
    private static final String TAG_IS = "is";

    private int mId;
    private String mName;
    private double mPrice;
    private String mImg;
    private int mIs;

    public Goods() {
    }

    // Return json objects of goods
    public Goods(JSONObject obj) throws JSONException {
        mId = obj.getInt(TAG_ID);
        mName = obj.getString(TAG_NAME);
        mPrice = obj.getDouble(TAG_PRICE);
        mImg = obj.getString(TAG_IMG);
        mIs = obj.getInt(TAG_IS);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public String getImg() {
        return mImg;
    }

    public void setImg(String img) {
        mImg = img;
    }

    public int getIs() {
        return mIs;
    }

    public void setIs(int is) {
        mIs = is;
    }
}
